import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/doctor-api";
import {Col, Label, Row} from "reactstrap";
import Select from "react-select";
import {Container} from "react-bootstrap";

const options = [
    { value: '08:00-10:00', label: '08:00 - 10:00' },
    { value: '10:00-12:00', label: '10:00 - 12:00' },
    { value: '12:00-14:00', label: '12:00 - 14:00' },
    { value: '14:00-16:00', label: '14:00 - 16:00' },
    { value: '16:00-18:00', label: '16:00 - 18:00' },
    { value: '18:00-20:00', label: '18:00 - 20:00' },
    { value: '20:00-22:00', label: '20:00 - 22:00' }
]

const textStyle = {color: 'darkcyan',  fontSize: 28,textAlign: 'left'};

const columns = [

    {
        Header: 'Id',
        accessor: 'id',
        show: false,
    },

    {
        Header: 'Medication Name',
        accessor: 'name',
        width: 150,
    },
    {
        Header: 'Side Effects',
        accessor: 'sideEffects',
        width: 200,
    },
    {
        Header: 'Dosage',
        accessor: 'dosage',
        width: 100,
    },

    {
        Cell: props=>{
            return(
                <button style={{backgroundColor: "darkcyan", color:"#fefefe"}}  onClick={()=>{window.lastPart.addMed(props.original.name)}}>Add Medication To Plan</button>

            )
        },
        width: 200,
        maxWidth: 200,
        minWidth: 200,
    },


];


const filters1 = [
    {
        accessor: 'name'
    }
];

class MedicationsTable1 extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            tableMedicationData: this.props.tableMedicationData,
            updatedIdMeds: [],
            informationMeds: [],
            collapseForm4: false,
            selected4: false,
            selectedOption: false,
            medPlanId: this.props.medPlanId
        };


        window.lastPart=this;
        console.log("Id" + this.state.medPlanId);
    }

    handleChange2 = selectedOption => {
        this.setState({ selectedOption });
    };

    render() {
        return (
            <div>
                <Row>
                <Table
                    data={this.state.tableMedicationData}
                    columns={columns}
                    search={filters1}
                    pageSize={5}
                />
                </Row>
                <br/> <br/>
                <Row>
                    <Col>
                        <Label style={textStyle}> Administration Interval: </Label>
                    </Col>
                    <Col sm={{size: '4', offset: 0}}>
                        <Select options = {options} onChange={this.handleChange2} value ={this.state.selectedOption} />
                    </Col>
                </Row>

                <br/><br/>

            </div>

        )
    }

    addMed(name) {
        return API_USERS.addMedToPlan(this.state.medPlanId,name,this.state.selectedOption.value,(result, status, err) => {

            if (result !== null && (status === 200 || status ===204 || status===202))
            {
                console.log("succes");

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }
}

export default MedicationsTable1;