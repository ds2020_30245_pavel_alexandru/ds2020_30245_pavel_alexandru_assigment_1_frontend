import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import {Card, Container, Jumbotron} from "react-bootstrap";
import MedicationsTable1 from "./medications1-table";
import BackgroundImg from "../../commons/images/animated4.gif";

const textStyle = {color: 'white',  fontSize: 28,textAlign: 'left'};

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'repeat',
    width: "100%",
    height: "900px",
    backgroundImage: `url(${BackgroundImg})`
};





class AddMedicationPlanForm extends React.Component {

    constructor(props) {

        super(props);

        this.toggleForm7=this.toggleForm7.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,
            patientId: this.props.updatedId,
            medPlanId :[],
            isLoaded9 : false,
            tableMedicationData:[],
            selectedOption:null,

            formControls: {
                startDate: {
                    value: '',
                    placeholder: 'YYYY-mm-dd',
                    touched: false,
                },
                endDate: {
                    value: '',
                    placeholder: 'YYYY-mm-dd',
                    touched: false,
                },
            }
        };

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;


        this.setState({
            formControls: updatedControls,
        });

    };

    addMedPlan(medPlan) {
        return API_USERS.addMedicationPlan(medPlan, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medplan with id: " + result);
                this.state.medPlanId=result;
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    findMeds()
    {
        return API_USERS.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableMedicationData: result,
                    isLoaded9: true,
                });
                console.log(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm7()
    {
        let medPlan = {
            patientId : this.state.patientId,
            meds : "",
            medsTime : "",
            startDate : this.state.formControls.startDate.value,
            endDate: this.state.formControls.endDate.value,
        };

        this.addMedPlan(medPlan);
        console.log("id::::"+this.state.medPlanId);
        this.findMeds();

    }



    render() {
        return (
            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                <FormGroup id='startDate'>
                    <Label for='startDateField' style={textStyle}> Medication Plan Start Date: </Label>
                    <Input name='startDate' id='startDateField' placeholder={this.state.formControls.startDate.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.startDate.value}
                           touched={this.state.formControls.startDate.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='endDate'>
                    <Label for='endDateField' style={textStyle}> Medication Plan End Date: </Label>
                    <Input name='endDate' id='endDateField' placeholder={this.state.formControls.endDate.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.endDate.value}
                           touched={this.state.formControls.endDate.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={this.toggleForm7}>  Create Medication Plan </Button>
                    </Col>
                </Row>
                <br/><br/>



                <Card>
                    <br/>
                    <Row>
                        <Col sm={{offset: 1}}>
                            {this.state.isLoaded9 && <MedicationsTable1 tableMedicationData = {this.state.tableMedicationData} medPlanId={this.state.medPlanId}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>

                </Card>

                    <br/>



                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }

                    </Container>
                </Jumbotron>
            </div>
        ) ;
    }


}

export default AddMedicationPlanForm;
