import React from "react";
import * as Moment from "moment";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/doctor-api";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import UpdatePatientForm from "./updatepatient-form";


const columns = [

    {
        Header: 'Id',
        accessor: 'id',
        show: false,

    },

    {
        Header: 'Name',
        accessor: 'name',
        width: 150,
    },
    {
        Header: 'Date of birth',
        id: 'dob',
        width: 150,
        accessor: d => {
            return Moment(d.dob)
                .local()
                .format("DD-MM-YYYY")
        }

    },
    {
        Header: 'Gender',
        accessor: 'gender',
        width: 150,
    },
    {
        Header: 'Address',
        accessor: 'address',
        width: 150,
    },
    {
        Header: 'Medical Record',
        accessor: 'medRecord',
        width: 150,
    },
    {
        Cell: props=>{

            return(
                <button style={{backgroundColor: "green", color:"#fefefe"}}
                        onClick={()=>{window.thisViewThird.addPatient(props.original.id)}}>Add to Caregiver</button>
            )
        },
        width: 200,
        maxWidth: 200,
        minWidth: 200
    },
];


const filters1 = [
    {
        accessor: 'name'
    }
];

class PatientsTable3 extends React.Component {

    constructor(props) {
        super(props);

        this.reloaderr=this.props.reloaderr;

        this.state = {
            idGet: this.props.idGet,
            patientsALL: this.props.allPatientsTable,
            errorStatus: 0,
            error: null
        };

        console.log("Second Table " + this.state.patientsALL);
        window.thisViewThird=this;
    }

    render() {

        return (
            <div>
                <Table
                    data={this.state.patientsALL}
                    columns={columns}
                    search={filters1}
                    pageSize={5}
                />
            </div>
        )
    }

    addPatient(idPatient) {
        return API_USERS.addPatientToCaregiver(this.state.idGet,idPatient,(result, status, err) => {

            if (result !== null && (status === 200 || status ===204 || status===202))
            {
                this.reloaderr();

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }
}

export default PatientsTable3;