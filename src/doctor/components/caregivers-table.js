import React from "react";
import Table from "../../commons/tables/table";
import * as Moment from "moment";
import * as API_USERS from "../api/doctor-api";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import UpdatePatientForm from "./updatepatient-form";
import UpdateCaregiverForm from "./updatecaregiver-form";
import ManagePatientsForm from "./managepatients-form";


const columns = [

    {
        Header: 'Id',
        accessor: 'id',
        show: false,
    },
    
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Date of birth',
        id: 'dob',
        accessor: d => {
            return Moment(d.dob)
                .local()
                .format("DD-MM-YYYY")
        }
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Cell: props=>{
            return(
                <button style={{backgroundColor: "red", color:"#fefefe"}} onClick={()=>{window.thisViewCaregiver.deleteCaregiverId(props.original.id)}}>Delete</button>
            )
        },
        width: 100,
        maxWidth: 100,
        minWidth: 100
    },
    {
        Cell: props=>{
            return(
                <button style={{backgroundColor: "green", color:"#fefefe"}} onClick={()=>{window.thisViewCaregiver.toggleFormCaregiver(props.original.id,props.original.name,props.original.dob,props.original.gender,props.original.address)}}>Update</button>
            )
        },
        width: 100,
        maxWidth: 100,
        minWidth: 100
    },
    {
        Cell: props=>{
            return(
                <button style={{backgroundColor: "darkcyan", color:"#fefefe"}}
                        onClick={()=>{window.thisViewCaregiver.toggleFormPatientsManagement(props.original.id)}}>Manage Patients</button>
            )
        },
        width: 150,
        maxWidth: 150,
        minWidth: 150
    }
];

const filters1 = [
    {
        accessor: 'name'
    }
];

class CaregiversTable extends React.Component {

    constructor(props) {
        super(props);

        this.reloadHandlerTableCaregiver=this.props.reloadHandlerTableCaregiver
        this.reloadOnlyCaregiver=this.props.reloadOnlyCaregiver
        this.toggleFormPatientsManagement=this.toggleFormPatientsManagement.bind(this);
        this.toggleFormCaregiver=this.toggleFormCaregiver.bind(this);

        this.state = {
            tableCaregiverData: this.props.tableCaregiverData,
            selected2: false,
            updatedId:[],
            information:[],
            collapseForm2: false,
            currentId:[],
            selectedPat: false,
        };


        window.thisViewCaregiver=this;
    }

    toggleFormCaregiver(id,name,dob,gender,address)
    {
        let caregiver = {
            name:name,
            dob: dob,
            gender: gender,
            address: address,
        };

        this.state.updatedId=id;
        this.state.information=caregiver;

        this.setState({selected2: !this.state.selected2});
    }

    toggleFormPatientsManagement(id) {

        this.state.currentId=id;
        this.setState({selectedPat: !this.state.selectedPat});
    }

    render() {
        return (
            <div>
            <Table
                data={this.state.tableCaregiverData}
                columns={columns}
                search={filters1}
                pageSize={5}
            />
                <Modal isOpen={this.state.selected2} toggle={this.toggleFormCaregiver}
                       className={this.props.className} size="lg" >
                    <ModalHeader toggle={this.toggleFormCaregiver}> Edit Caregiver Data: </ModalHeader>
                    <ModalBody>
                        <UpdateCaregiverForm informationCaregiver = {this.state.information} updatedIdCaregiver = {this.state.updatedId} reloadForUpdate = {this.reloadHandlerTableCaregiver}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedPat} toggle={this.toggleFormPatientsManagement}
                       className={this.props.className} size="xl" >
                    <ModalHeader toggle={this.toggleFormPatientsManagement}> Manage Caregiver's Patients: </ModalHeader>
                    <ModalBody>
                        <ManagePatientsForm currentId = {this.state.currentId} />
                    </ModalBody>
                </Modal>

            </div>
        )
    }

    deleteCaregiverId(id) {
        return API_USERS.deleteCaregiver(id,(result, status, err) => {

            if (result !== null && (status === 200 || status ===204 || status===202))
            {
                console.log("Deleted caregiver with id:",id);
                this.deleteUserId(id);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deleteUserId(id) {
        return API_USERS.deleteUser(id,(result, status, err) => {

            if (result !== null && (status === 200 || status ===204 || status===202))
            {
                console.log("Deleted user linked to account id:",id);
                this.reloadHandlerTableCaregiver();

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


}

export default CaregiversTable;