import React from "react";
import * as Moment from "moment";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/doctor-api";


const columns = [

    {
        Header: 'Id',
        accessor: 'id',
        show: false,
    },

    {
        Header: 'Name',
        accessor: 'name',
        width: 150,
    },
    {
        Header: 'Date of birth',
        id: 'dob',
        width: 150,
        accessor: d => {
            return Moment(d.dob)
                .local()
                .format("DD-MM-YYYY")
        }
    },
    {
        Header: 'Gender',
        accessor: 'gender',
        width: 150,
    },
    {
        Header: 'Address',
        accessor: 'address',
        width: 150,
    },
    {
        Header: 'Medical Record',
        accessor: 'medRecord',
        width: 150,
    },
    {
        Cell: props=>{

            return(
                <button style={{backgroundColor: "red", color:"#fefefe"}}
                        onClick={()=>{window.thisViewPatientsTable2.removePatient(props.original.id)}}>Remove from Caregiver</button>
            )
        },
        width: 200,
        maxWidth: 200,
        minWidth: 200,
    },
];


const filters1 = [
    {
        accessor: 'name'
    }
];

class PatientsTable2 extends React.Component {

    constructor(props) {
        super(props);

        this.reloaderr=this.props.reloaderr;

        this.state = {
            tableDataCustom: this.props.tablePatientDataCaregiver,
            errorStatus: 0,
            error: null,
            idGet: this.props.idGet
        };

        console.log("First Table " + this.state.tableDataCustom);
        window.thisViewPatientsTable2=this;

    }


    render() {

        return (
            <div>
                <Table
                    data={this.state.tableDataCustom}
                    columns={columns}
                    search={filters1}
                    pageSize={5}
                />
            </div>
        )
    }

    removePatient(id) {
        return API_USERS.removePatientFromCaregiver(this.state.idGet,id,(result, status, err) => {

            if (result !== null && (status === 200 || status ===204 || status===202))
            {
                this.reloaderr();

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }
}

export default PatientsTable2;