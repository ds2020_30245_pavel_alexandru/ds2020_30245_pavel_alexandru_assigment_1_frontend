import React from 'react';
import {ModalBody, Modal, ModalHeader, CardHeader} from "reactstrap";
import * as API_USERS from "./api/doctor-api";
import BackgroundImg from "../commons/images/animated4.gif";
import * as Moment from "moment";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import CaregiversTable from "./components/caregivers-table";
import MedicationsTable from "./components/medications-table";
import PatientsTable1 from "./components/patients-table";
import AddPatientForm from "./components/addpatient-form";
import {Card, Col, Container, Jumbotron, Row} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import AddMedicationForm from "./components/addmedication-form";
import AddCaregiverForm from "./components/addcaregiver-form";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'repeat',
    width: "100%",
    height: "3000px",
    backgroundImage: `url(${BackgroundImg})`
};

const title = {color: 'white',  fontSize: 100,textAlign: 'center'};
const textStyle = {color: 'white',  fontSize: 42,textAlign: 'left'};


class DoctorContainer extends React.Component {

    constructor(props) {
        super(props);
        window.acces=this;
        this.toggleForm1 = this.toggleForm1.bind(this);
        this.toggleFormNew = this.toggleFormNew.bind(this);
        this.toggleFormCaregivers= this.toggleFormCaregivers.bind(this);
        this.reload = this.reload.bind(this);
        this.reloadSecondary = this.reloadSecondary.bind(this);
        this.reloadThird = this.reloadThird.bind(this);
        this.reloadCaregivers= this.reloadCaregivers.bind(this);
        this. reloadCaregiversNew=this. reloadCaregiversNew.bind(this);
        this. reloadPatientsNew=this. reloadPatientsNew.bind(this);
        this. reloadMedicationNew=this. reloadMedicationNew.bind(this);

        this.state = {
            selected1: false,
            selected2: false,
            selected5: false,
            collapseForm: false,
            collapseForm1: false,
            doctorData: [],
            tablePatientData: [],
            tableCaregiverData: [],
            tableMedicationData: [],
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            errorStatus: 0,
            error: null
        };

    }

    fetchPatients() {

        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tablePatientData: result,
                    isLoaded1: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchCaregivers() {

        return API_USERS.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableCaregiverData: result,
                    isLoaded2: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleFormNew()
    {
        this.setState({selected2: !this.state.selected2});
    }

    toggleFormCaregivers()
    {
        this.setState({selected5: !this.state.selected5});
    }

    fetchMedications() {

        return API_USERS.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableMedicationData: result,
                    isLoaded3: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    componentDidMount() {
        this.getAccountData();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
    }

    getAccountData() {

        return API_USERS.getDoctor(window.sessionStorage.getItem("accountID"),(result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    doctorData: result,
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm1() {
        this.setState({selected1: !this.state.selected1});
    }

    reload() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false
        });
        this.getAccountData();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
    }

    reloadSecondary() {
        this.setState({
            isLoaded1: false
        });
        this.fetchPatients();
        this.toggleForm1();
    }

    reloadThird() {
        this.setState({
            isLoaded3: false
        });
        this.fetchMedications();
        this.toggleFormNew();
    }

    reloadCaregivers()
    {
        this.setState({
            isLoaded2: false
        });
        this.fetchCaregivers();
        this.toggleFormCaregivers();
    }

    reloadCaregiversNew()
    {
        this.setState({
            isLoaded2: false
        });
        this.fetchCaregivers();
    }

    reloadPatientsNew()
    {
        this.setState({
            isLoaded1: false
        });
        this.fetchPatients();
    }

    reloadMedicationNew()
    {
        this.setState({
            isLoaded3: false
        });
        this.fetchMedications();
    }

    render() {

        Moment.locale('en');
        return (
            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={title}>Welcome, Dr. {this.state.doctorData.name}</h1>
                        <br /><h1 className="display-3" style={textStyle}>Registered Patients</h1><br />
                        <CardHeader>
                            <strong> </strong>
                        </CardHeader>
                        <Card>
                            <br/>
                            <Row>
                                <Col sm={{offset: 0}}>
                                {this.state.isLoaded1 && <PatientsTable1 tablePatientData = {this.state.tablePatientData} reloadHandlerTable = {this.reloadPatientsNew} />}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />   }
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={{size: '4', offset: 1}}>
                                    <Button color="primary" onClick={this.toggleForm1}>New Patient </Button>
                                </Col>
                            </Row>
                        </Card>
                                <br />
                                <h1 className="display-3" style={textStyle}>Registered Caregivers</h1>
                                <br />
                        <CardHeader>
                            <strong> </strong>
                        </CardHeader>
                        <Card>
                            <br/>
                            <Row>
                                <Col sm={{offset: 0}}>
                                {this.state.isLoaded2 && <CaregiversTable reloadHandlerTableCaregiver={this.reloadCaregiversNew} tableCaregiverData = {this.state.tableCaregiverData}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />   }
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={{size: '4', offset: 1}}>
                                    <Button color="primary" onClick={this.toggleFormCaregivers}>New Caregiver </Button>
                                </Col>
                            </Row>

                        </Card>

                                <br />
                                <h1 className="display-3" style={textStyle}>Registered Medications</h1>
                                <br />
                        <CardHeader>
                            <strong> </strong>
                        </CardHeader>
                        <Card>
                            <br/>
                            <Row>
                                <Col sm={{offset: 0}}>
                                {this.state.isLoaded3 && <MedicationsTable tableMedicationData = {this.state.tableMedicationData} reloadHandlerTableMedication={this.reloadMedicationNew}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />   }
                                <br />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={{size: '4', offset: 1}}>
                                    <Button color="primary" onClick={this.toggleFormNew}>New Medication </Button>
                                </Col>
                            </Row>
                        </Card>

                    </Container>
                </Jumbotron>

                <Modal isOpen={this.state.selected1} toggle={this.toggleForm1}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm1}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <AddPatientForm reloadHandler={this.reloadSecondary}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected2} toggle={this.toggleFormNew}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormNew}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <AddMedicationForm reloadHandler={this.reloadThird}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected5} toggle={this.toggleFormCaregivers}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormCaregivers}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <AddCaregiverForm reloadHandler={this.reloadCaregivers}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}
export default DoctorContainer;

