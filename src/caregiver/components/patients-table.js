import React from "react";
import Table from "../../commons/tables/table";
import * as Moment from "moment";


const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Date of birth',
        id: 'dob',
        accessor: d => {
            return Moment(d.dob)
                .local()
                .format("DD-MM-YYYY")
        }
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Header: 'Medical Record',
        accessor: 'medRecord',
    }
];

const filters1 = [
    {
        accessor: 'name'
    }
];

class PatientsTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters1}
                pageSize={5}
            />
        )
    }
}

export default PatientsTable;